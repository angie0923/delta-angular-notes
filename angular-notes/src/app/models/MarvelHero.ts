export interface MarvelHero {
  persona: string,
  firstName: string,
  lastName: string,
  age: number,
  img?: string
}
