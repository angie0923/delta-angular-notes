export interface Athlete {
 name: string,
 team: string,
 jerseyNo: number,
 stillPlaying: boolean
}
