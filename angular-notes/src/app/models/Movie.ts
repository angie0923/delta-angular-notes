export interface Movie {
  title: string,
  yearReleased: number,
  actors: string,
  director: string,
  genre: string,
  rating: string,
  awards?: string

}
