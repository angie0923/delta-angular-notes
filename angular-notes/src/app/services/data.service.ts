import { Injectable } from '@angular/core';
import {DCHero} from "../models/DCHero";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DataService {
  //properties

  // initialize the characters property and assign it
  // to an array of our DCHero interface

  characters: DCHero[] = [];


  constructor() {
    this.characters = [
      {
        persona: "Superman",
        firstName: "Clark",
        lastName: "Kent",
        age: 54,
        address:{
          street: `27 Smallville`,
          city: "Metropolis",
          state: "IL"
        },
        img: "../assets/img/superman.png",
        isActive: true,
        balance: 12000000,
        memberSince: new Date("05/01/1939 8:30:00"),
        hide: false
      },
      {
        persona: "Wonder Woman",
        firstName: "Diana",
        lastName: "Prince",
        age: 5000,
        address:{
          street: `150 Main St`,
          city: "Los Angeles",
          state: "CA"
        },
        img: "../assets/img/wonderwoman.png",

        balance: 14000000,
        memberSince: new Date("07/11/2005 10:45:00"),
        hide: false
      },
      {
        persona: "Batman",
        firstName: "Bruce",
        lastName: "Wayne",
        age: 43,
        address:{
          street: `50 Wayne Manor`,
          city: "Gotham City",
          state: "NJ"
        },
        img: "../assets/img/batman.png",
        isActive: true,
        balance: 20000000,
        memberSince: new Date("08/07/1979 12:00:00"),
        hide: false
      }

    ]; // end of array
  } // end of constructor

  // Methods go here:

  // create a method that will return an array of this.characters
  // using an observable

  // OBSERVABLES:
  /*
 - provides support for passing messages / data between parts of your application
 - Used frequently and is a technique for event handling, asynchronous
   programming and handling multiple values.
   */

  getCharacters() : Observable<DCHero[]> {
    console.log("Getting characters from dataService")
    return of(this.characters)
  }










} // end of class
