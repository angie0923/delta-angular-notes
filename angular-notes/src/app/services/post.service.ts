import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {PostHttpClient} from "../models/PostHttpClient";
import {Observable} from "rxjs";

const httpOptions = {
  headers: new HttpHeaders({"Content-Type": "application/json"})
}

@Injectable({
  providedIn: 'root'
})
export class PostService {

  postsUrl: string = "https://jsonplaceholder.typicode.com/posts";

  // inject the HttpClientModule as a dependency
  constructor(private httpClient: HttpClient) { }

  // create a method that will make our GET request to the postsUrl
  // use observable method

  getPosts(): Observable<PostHttpClient[]> {

    // returning all the data that comes with the postUrl property
    return this.httpClient.get<PostHttpClient[]>(this.postsUrl)
  }

  // create a method that will make a POST request to the postsUrl property

  savePost(post: PostHttpClient) : Observable<PostHttpClient>{
    return this.httpClient.post<PostHttpClient>(this.postsUrl, post, httpOptions);
  }

  // create a method that will make a PUT request to the postsUrl property

  updatePost(postUpdated: PostHttpClient) : Observable<PostHttpClient>{
    const url = `${this.postsUrl}/${postUpdated.id}`;

    return this.httpClient.put<PostHttpClient>(url, postUpdated, httpOptions)
  }
  // created a new const variable 'url' - this is taking the json url and the id of the selected post
  // returning everything as a PUT request, not as a POST
  // "PUTTING" this on the 'const url' NOT on the 'this.postsUrl'


  // create a method that will make a DELETE request to the url

  deletePost(postDeleted: PostHttpClient | number): Observable<PostHttpClient> {
    const id = typeof postDeleted === `number`? postDeleted : postDeleted.id;
    /*
    if whats being passed in is a type of number its postDeleted ( our perameter )
    otherwise use the id of 'postDeleted'
    ? postDeleted : postDeleted.id;  (is a terinary operator)
     */
    const url = `${this.postsUrl}/${id}`
    return this.httpClient.delete<PostHttpClient>(url, httpOptions);
  }

}


