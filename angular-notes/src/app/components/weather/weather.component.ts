import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit {
 // properties go here
  goodWeather: string = "sunny and cool!";
  badWeather: string = "cold and snowy!";
  constructor() { }

  ngOnInit(): void {
    // call the display good weather message method
    // this.displayGoodWeatherMessage();

    // call display bad weather message method
    this.displayBadWeatherMessage();
  }

  // methods go here
  displayGoodWeatherMessage(){
    alert(`Today's weather is ${this.goodWeather}`);
  }
  displayBadWeatherMessage(){
    alert(`Bundle up, charge your phones, because today's weather is ${this.badWeather}`)
  }

}
