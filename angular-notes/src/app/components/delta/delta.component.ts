import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-delta',
  templateUrl: './delta.component.html',
  styleUrls: ['./delta.component.css']
})
export class DeltaComponent implements OnInit {
  // properties - attribute of a component
  // properties go above constructor
  firstName: string = "Delta";
  lastName: string = "Cohort";
  year: number = 2021;


  // constructor method is called when component is initialized (used).
  // used for dependency injections - connection from the service(s).
  constructor() { }

  // called when component is initialized
  // used for reassigning properties and calling methods
  ngOnInit(): void {
    console.log("Hello from Delta Component.....");
    console.log(`${this.firstName} ${this.lastName}`);
    // in order to access properties, we need to use the "this" keyword

    this.greeting();
    // in order to call a method use the "this" keyword.
  }

  // Methods - function inside of a component's class
  greeting(){
    alert("Hello there, " + this.firstName);
    this.displayLastName(); // calling display last name here will give us the last name after
    // the first name and before the year. using "this" keyword
    alert("The year is " + this.year);
    // *NOTE: to access properties in a method, use the "this" keyword
    // "greeting" must be called in the ngOnInIt under the console.logs -- this.greeting();
  }

  displayLastName(){
    alert("Last name is " + this.lastName);
    // if we want the last name to show up after the first name we call the method after the first
    // name alert in greeting using "this" keyword
  }



} // end of the class
