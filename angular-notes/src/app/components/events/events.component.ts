import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  // properties

  showForm: boolean = true;  // shows the form

  constructor() { }

  ngOnInit(): void {
    this.showForm = false;  //default to hide form
  } // end of ngOnInIt

  // Methods
  // method will set showForm to true / false
  toggleForm(){
    console.log("Toggle form button was clicked")
    this.showForm = !this.showForm;   // either show/hide the form
  }

  // console.log toggle button to test if button works

  // method for the different types of mouse events
  triggerEvent(event: any){
    console.log(event.type);
  }


}



























































