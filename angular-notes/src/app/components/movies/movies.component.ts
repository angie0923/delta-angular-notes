import { Component, OnInit } from '@angular/core';
import {Movie} from "../../models/Movie";

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  // properties
  movies: Movie[] = [];
  currentStyle: {} = {};
  currentStyle1: {} = {};



  constructor() { }

  ngOnInit(): void {
    this.movies = [
      {
        title: "Scream",
        yearReleased: 1996,
        actors: "Neve Campbell, David Arquette, Courteney Cox",
        director: "Wes Craven",
        genre: "Horror, Mystery",
        rating: "R",
        awards: "Best Director, Best Actress, Best Horror Film"
      },
      {
        title: "Halloween H20",
        yearReleased: 1998,
        actors: "Jaime Lee Curtis, Josh Hartnett, Michelle Williams",
        director: "Steve Miner",
        genre: "Horror, Thriller",
        rating: "R",
        awards: "Best Horror Film, Best Actress, Best Original Score"
      },
      {
        title: "Texas Chainsaw Massacre",
        yearReleased: 2003,
        actors: "Jessica Biel, Jonathan Tucker, Andrew Bryniarski",
        director: "Marcus Nispel",
        genre: "Horror, Crime",
        rating: "R",
        awards: "Best Horror Film, Best Actress, Best Villain"
      },

    ] // end of array

    this.addMovie()
    this.setCurrentStyle()
    this.setCurrentStyle1()


  } // end of ngOnInIt

  //METHODS
  showMessage(){
    return "This movie has an 'R' rating!"
  }

  showPGMessage(){
    return "This movie has a 'PG' rating!"
  }

  addMovie(){
    this.movies.push({title: "Coraline", yearReleased: 2009, actors:"Dakota Fanning, Teri Hatcher, Robert Bailey", director: "Henry Selick", genre: "Animation", rating: "PG",awards: "Best Animated Film, Best Feature, Movie of the Year" });
  }

  setCurrentStyle (){
    this.currentStyle = {
      "background-color" : "red"
    }
  }

  setCurrentStyle1 (){
    this.currentStyle1 = {
      "background-color" : "yellow"
    }
  }



}
