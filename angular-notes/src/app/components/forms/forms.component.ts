import {Component, OnInit, ViewChild} from '@angular/core';
import {DCHero} from "../../models/DCHero";
import {DataService} from "../../services/data.service";

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit {

  // properties

  characters: DCHero[] = [];
  currentClasses: {} = {}; // empty object
  currentStyle: {} = {};
  enableAddCharacter: boolean = true;
// character empty object
  character: DCHero = {
    persona: '',
    firstName: '',
    lastName: '',
    age: 0,
    address: {
      street: '',
      city: '',
      state: '',
    },
    img: '',
    isActive: true,
    memberSince: '',
    hide: true
  }


  // submit lesson
  @ViewChild("characterForm") form: any;
  /*
  calling ViewChild and passed in the name of our form -- "characterForm"
  - making this our Form Identifier"
   */

  // inject the data service in the constructor
  // Dependency injection
  // private = cannot be used anywhere else, only within this class
  // dataService is the variable name for our DataService
  // DataService = setting our variable to the DataService that we brought
  // in, now we should be able to access any method(s) inside of our DataService.

  constructor(private dataService: DataService) { }

  ngOnInit(): void {


    this.setCurrentClasses();
    this.setCurrentStyle();

    // access the getCharacters() method that's inside of our DataService
    // when we are accessing an observable method, we need to subscribe to it.
    this.dataService.getCharacters().subscribe(data => {
      this.characters = data;
    });

  } // end of ngOnInIt()

  // Method
  // example of ngClass

  setCurrentClasses(){
    // calling and updating the property currentClasses
    this.currentClasses = {
      'btn-success' : this.enableAddCharacter
    }
  }

  // example of ngStyle

  setCurrentStyle(){
    // call and update the property currentStyle
    this.currentStyle = {
      "padding-top" : "60px",
      "text-decoration" : "underline"
    }
  }

  // METHOD to toggle individual character's info

  toggleInfo(character: any){
    console.log("toggle info clicked")
    character.hide = !character.hide
  }

  // onSubmit
  onSubmit({value, valid} : {value: DCHero, valid: boolean}) {
   if (!valid){
     alert("Form is not valid")
   }
   else {
     value.isActive = true;
     value.memberSince = new Date();
     value.hide = false;
     console.log(value);
     // add new data to top of the list
     this.characters.unshift(value);

     // reset the form / clear out the inputs
     this.form.reset();

   }
  }


}
