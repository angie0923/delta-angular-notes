import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {PostHttpClient} from "../../models/PostHttpClient";
import {PostService} from "../../services/post.service";

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {
  // properties
  // created new property that's referencing our interface
  // set values to 0 or ''

  post: PostHttpClient = {
    id: 0,
    title: '',
    body: ''
  };

  //
  @Output() newPost: EventEmitter<PostHttpClient> = new EventEmitter();

  @Output() updatedPost: EventEmitter<PostHttpClient> = new EventEmitter();


  // defining currentPost as an "Input"
  @Input() currentPost: PostHttpClient = {
    id: 0,
    title: '',
    body: ''
  };

  // defining "isEditing" as another input
  @Input() isEditing: boolean = false;


  // inject our dependencies
  constructor(private postService: PostService) { }

  ngOnInit(): void {
  } // end of ngOnInIt

  // create a method named addPost() by using the PostService savePost() and
  // attaching the event emitter
  addPost(title: any, body: any) {
    if (!title || !body){
      alert("Please complete the form")
    }
    else {
      console.log(title, body);
      this.postService.savePost({title, body} as PostHttpClient)
        .subscribe(data => {
          this.newPost.emit(data);
        })
    }
  }

  // create a new method called "updatePost()" that will subscribe to the Observable method in our "service"

  updatePost(){
    console.log("Updating post....");
    this.postService.updatePost(this.currentPost)
      .subscribe(data => {
        this.isEditing = false; // resets button back to 'submit post'
        this.updatedPost.emit(data);
      })
    this.currentPost = {
      id: 0,
      title: "",
      body: ""
    }
  }


} // end of class





























































