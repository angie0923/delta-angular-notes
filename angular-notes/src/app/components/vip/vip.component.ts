import { Component, OnInit } from '@angular/core';
import {Member} from "../../models/Member";

@Component({
  selector: 'app-vip',
  templateUrl: './vip.component.html',
  styleUrls: ['./vip.component.css']
})
export class VipComponent implements OnInit {
  // properties
  members: Member [] = []; // points to our member interface

  // bonus
  loadingMembers: boolean = false;

  constructor() { }

  ngOnInit(): void {
    // bonus
    setTimeout(()=> {this.loadingMembers = true}, 5000)


    // how do we access a property inside of a method?  "this"
    this.members =[
      {
        firstName: "Davey",
        lastName: "Havok",
        userName: "AFireInside",
        memberNo: 1645
      },
      {
        firstName: "Billy",
        lastName: "Corgan",
        userName: "SmashingPumpkins",
        memberNo: 1234
      },
      {
        firstName: "Brian",
        lastName: "Warner",
        userName: "MarilynManson",
        memberNo: 1666
      },
      {
        firstName: "Anthony",
        lastName: "Kiedis",
        userName: "RHCPRocks!",
        memberNo: 1983
      },
      {
        firstName: "Gustav",
        lastName: "Ahr",
        userName: "LilPeep",
        memberNo: 1115
      }

    ] // end of members array
  } // end of ngOnInIt


}



















































































