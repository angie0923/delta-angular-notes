import { Component, OnInit } from '@angular/core';
import {Athlete} from "../../models/Athlete";

@Component({
  selector: 'app-directives',
  templateUrl: './directives.component.html',
  styleUrls: ['./directives.component.css']
})
export class DirectivesComponent implements OnInit {
  // properties
  athletes: Athlete[] = [];  // pointing to athlete interface



  constructor() { }

  ngOnInit(): void {
    // how do we access a property inside of a method?  "this"
    this.athletes = [
      {
        name: "Michael Jordan",
        team: "Chicago Bulls",
        jerseyNo: 23,
        stillPlaying: false
      },
      {
        name: "LeBron James",
        team: "Lakers",
        jerseyNo: 23,
        stillPlaying: true
      },
      {
        name: "Lionel Messi",
        team: "Barcelona",
        jerseyNo: 10,
        stillPlaying: true
      },
      {
        name: "Alex Morgan",
        team: "Orlando Pride",
        jerseyNo: 13,
        stillPlaying: true
      }
    ] // end of athletes array

  } // end of ngOnInIt
// METHOD
  showStatus(){
    return "This athlete is currently playing ";
  }
  showRetired(){
    return "This athlete is retired"
  }

}
