import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from "./components/home/home.component";
import { CommentsComponent } from "./components/comments/comments.component";
import { PostHttpClient } from "./models/PostHttpClient";
import { TravelComponent } from "./components/travel/travel.component";
import { PostHttpClientComponent } from "./components/post-http-client/post-http-client.component";
import { MoviesComponent } from "./components/movies/movies.component";

// this is where we're going to route our paths to the components
const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'comments',
    component: CommentsComponent
  },
  {
    path: 'posts',
    component: PostHttpClientComponent
  },
  {
    path: 'travel',
    component: TravelComponent
  },
  {
    path: 'movies',
    component: MoviesComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
